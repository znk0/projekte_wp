from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from raten.models import InputNumber

def index(request):
    return render(request, "index.html")

def AddNumber(request):
    guess_number = InputNumber(content = request.POST['content'])
    guess_number.save()
    return HttpResponse("¯\_(ツ)_/¯")