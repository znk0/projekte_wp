from django import forms
from shopping.models import List

class ListForm(forms.ModelForm):
	class Meta:
		model = List
		fields = ["item", "menge", "completed"]