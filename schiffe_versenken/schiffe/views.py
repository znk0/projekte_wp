from django.shortcuts import render
from django.http import HttpResponse
from .forms import UserForm

# Create your views here.
def setup(request):    
    form = UserForm()
    return render(request, 'setup.html', {'form': form})