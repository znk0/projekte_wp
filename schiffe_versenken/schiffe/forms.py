from django import forms

class UserForm(forms.Form):
    name = forms.CharField(label="Namen eingeben")
    category = forms.ChoiceField(choices[('abc', 'ABC'), ('def', 'DEF')])
    